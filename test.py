import metabolic_scope as ms  # type: ignore

seed = {"cpd1"}

reactions = {
    "v1": {
        "substrates": {"cpd1"},
        "products": {"cpd2"},
    },
    "v2": {
        "substrates": {"cpd2"},
        "products": {"cpd3"},
    },
}

scope_reactions, scope_compounds = ms.scope(seed, reactions)
assert scope_reactions == ["v1", "v2"]
assert scope_compounds == ["cpd2", "cpd3"]

print("Test run successfully")
