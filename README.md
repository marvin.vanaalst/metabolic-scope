# Metabolic scope

```bash
pip install maturin
maturin develop
python test.py
```

## Development setup

```bash
pip install gitlint
gitlint install-hook
```

Commit messages have to be formatted like this `(chg|fix|new): (dev|usr): .*`
