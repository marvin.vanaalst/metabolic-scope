use pyo3::prelude::*;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Clone)]
struct Reaction {
    substrates: HashSet<String>,
    products: HashSet<String>,
}

fn scope(
    seed: HashSet<String>,
    reactions: HashMap<String, Reaction>,
) -> (Vec<String>, Vec<String>) {
    let mut all_compounds = seed;
    let mut possible_reactions = reactions;
    let mut scope_reactions = Vec::new();
    let mut scope_compounds = Vec::new();
    loop {
        let mut reactions_round = HashSet::new();
        let mut compounds_round = HashSet::new();
        for (name, reaction) in possible_reactions.iter() {
            if reaction.substrates.is_subset(&all_compounds) {
                reactions_round.insert(name.clone());
                for product in reaction.products.iter() {
                    compounds_round.insert(product.clone());
                }
            }
        }
        if !reactions_round.is_empty() {
            let mut new_compounds = Vec::new();
            for cpd in compounds_round.difference(&all_compounds) {
                new_compounds.push(cpd.clone());
            }

            for cpd in new_compounds {
                all_compounds.insert(cpd.clone());
                scope_compounds.push(cpd);
            }
            for name in reactions_round {
                possible_reactions.remove(&name);
                scope_reactions.push(name);
            }
        } else {
            return (scope_reactions, scope_compounds);
        }
    }
}

#[pyfunction]
#[pyo3(name = "scope")]
fn scope_python(
    seed: HashSet<String>,
    reactions: HashMap<String, HashMap<String, HashSet<String>>>,
) -> (Vec<String>, Vec<String>) {
    let mut rust_reactions = HashMap::new();
    for (name, rxn) in reactions.iter() {
        let reaction = Reaction {
            substrates: rxn.get("substrates").unwrap().clone(),
            products: rxn.get("products").unwrap().clone(),
        };
        rust_reactions.insert(name.clone(), reaction);
    }
    return scope(seed, rust_reactions);
}

#[pymodule]
fn metabolic_scope(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(scope_python, m)?)?;
    Ok(())
}
